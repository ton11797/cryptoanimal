import {SignerWithAddress} from '@nomiclabs/hardhat-ethers/dist/src/signer-with-address';
import {BigNumber} from 'ethers';

export type User = {
  address: string;
  name: string;
  signer: SignerWithAddress;
  ownPlayerIds: string[];
};

export type Fixture = {
  deployer: User;
  boss: User;
  admin: User;
  user1: User;
  user2: User;
  user3: User;
  user4: User;
  user5: User;
};

export enum ContractId {
  PlayerBase = 'PlayerBase',
  GenomeControl = 'GenomeControl',
  PlayerCore = 'PlayerCore',
}

export interface ContractDeployResult {
  network: string;
  address: string;
  deployer: string;
}

export type DbSchema = Record<ContractId, ContractDeployResult>;

export enum ContractType {
  Contract = 'Contract',
}

export enum ProtocolErrors {
  ONLY_BOSS = 'OB',
  ONLY_ADMIN = 'OA',
  ONLY_CONTROL = 'OC',
  ADDRESS_ZEROP = 'A0',
}

export type ExpectedTxResult = TxSuccess | TxFail;

export type TxSuccess = Record<string, never>;

export type TxFail = {
  revertMessage: NonNullable<string>;
};

export type PlayerData = {
  chromosomeA: BigNumber;
  chromosomeB: BigNumber;
  chromosomeC: BigNumber;
  birthTime: BigNumber;
  cooldownEndBlock: BigNumber;
  partnerAId: BigNumber;
  partnerBId: BigNumber;
  pregnantWithId: BigNumber;
  cooldownIndex: BigNumber;
  generation: BigNumber;
};
