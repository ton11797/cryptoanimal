import {HardhatNetworkAccountsUserConfig} from 'hardhat/types';

const balance = '10000000000000000000000'; // 10,000ETH

export const testAccounts: HardhatNetworkAccountsUserConfig = [
  {privateKey: '0x95aa72f5defbe009febe627597ecac91a369569fa3c9d54873ab3c1d89661962', balance},
  {privateKey: '0xb5453348ee7c187d4fa3a4dbd46f39c044ab35ea1efa024e5d85a523e65495aa', balance},
  {privateKey: '0x33472673f11aa7b5b28dece47f0f2237ffcc72ab62aad8ba3390999edd646dfa', balance},
  {privateKey: '0xa46fc9a02e785c070d2d4055a3ec060cfa77945607c10b9d89dc3eaaaff6187f', balance},
  {privateKey: '0x680306b068d4d598e944b3b38e395483f94c658ba903f23085e5668195402845', balance},
  {privateKey: '0xc92d2ffe94d3c34b314ff375cda71ba4b6d730f5130fe7b11fa94f40c5a52eb4', balance},
  {privateKey: '0x45486fac41e3fdb1378e1f527e15b5ecb1863da1da283e439a8aa1732446103e', balance},
  {privateKey: '0xe18a576a993ff5ad65103fa19e713e8bc4ddc66016826053adba39241cdea822', balance},
  {privateKey: '0xe71803881cc5dae85d84d0076058c94e8f59496ac7e312c451198c809170f426', balance},
  {privateKey: '0x1b07da7fd1ff29b763b90a30ca34b139e25f0b167a77377a6f8304a36112e0a3', balance},
  {privateKey: '0x90123922d9eebfb83c7c662c48062387b73250c302b4f4e772cd9d03caa3e90d', balance},
  {privateKey: '0x12538598420c8dc3d3c4a5f102feb8ba49713ab16741a6e17bc324035e577666', balance},
  {privateKey: '0x94cc6c41624a2e7114a067b9714f3650640200d48a6ccc5984ad590b28e95d7b', balance},
  {privateKey: '0x09664e9a3ef486214409cc26023736edd9949ee76269f750a06f1f4ab2e6c002', balance},
  {privateKey: '0x51c7f51047260725946e368d3bc9a3b9ea93f160752fa2a1dc194ad8c916d0b0', balance},
  {privateKey: '0x00e3310c3c1f06d66006e5da4a2c95fa71b599372f01e89353d4fdd2fa103ff7', balance},
  {privateKey: '0x7bbb1378e5d2d74e9a253033077ea2659938e76e2cdc97068151ee8932cacdf6', balance},
  {privateKey: '0xfeb9f0ce27910b6ae9bfc6152f66358d2d5e733928ddbf3c608bad41da305cee', balance},
  {privateKey: '0x82045f2883aea49b5c3164ff6e82f6d8177b5b38d624ad73dc4bace4135800fd', balance},
  {privateKey: '0x53f801c6a46ffa6ebcb27452aa885b8e34372f8b178cc686c94998a5134d52cb', balance},
];
