import {DeployFunction} from 'hardhat-deploy/dist/types';
import {HardhatRuntimeEnvironment} from 'hardhat/types';
import {deployPlayer, deployGenome} from '../utils/contractDeployer';

const func: DeployFunction = async (hre: HardhatRuntimeEnvironment) => {
  const addressGenomeControl = await deployGenome();
  const addressPlayerCore = await deployPlayer();
  await addressPlayerCore.setGenomeControlAddress(addressGenomeControl.address);
};

export default func;
func.tags = ['testEnv'];
