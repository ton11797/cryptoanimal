// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

interface IGenomeControl {
    function isGenomeControl() external returns (bool);
    function mixChromosome(uint256 _chromosomeA, uint256 _chromosomeB, uint256 targetBlock) external returns (uint256);
}