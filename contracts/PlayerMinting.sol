//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;
import "./PlayerBreeding.sol";

/// @title A facet of PlayerCore that manages special access privileges.
/// @author Denice
/// @dev See the PlayerCore contract documentation to understand how the various contract facets are arranged.
contract PlayerMinting is PlayerBreeding{
    uint256 public promoCreatedCount;
    uint256 public constant PROMO_CREATION_LIMIT = 5000;

    function createPromoPlayer(uint256 _chromosomeA,uint256 _chromosomeB,uint256 _chromosomeC, address _owner) external onlyAdmin returns (uint playerId) {
        address playerOwner = _owner;
        if (playerOwner == address(0)) {
             playerOwner = adminAddress;
        }
        require(promoCreatedCount < PROMO_CREATION_LIMIT);

        promoCreatedCount++;
        playerId = _createPlayer(0, 0, 0, _chromosomeA, _chromosomeB, _chromosomeC, playerOwner);
    }
    
}