//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "./PlayerBase.sol";
import "./interface/IGenomeControl.sol";

/// @title A facet of PlayerCore that manages special access privileges.
/// @author Denice
/// @dev See the PlayerCore contract documentation to understand how the various contract facets are arranged.
contract PlayerBreeding is PlayerBase{
    event Pregnant(address owner, uint256 partnerAId, uint256 partnerBId, uint256 cooldownEndBlock);
    event Mutation(address genomeControlAddress);
    
    //Replace 1 finney for 1e15 (because 1 finney == 1000000000000000 wei).
    uint256 public autoBirthFee = 2 * 1e15;

    // Keeps track of number of pregnant kitties.
    uint256 public pregnantPlayers;

    IGenomeControl public genomeControl;

    function setAutoBirthFee(uint256 val) external onlyAdmin {
        autoBirthFee = val;
    }

    function setGenomeControlAddress(address _address) external onlyBoss {
        IGenomeControl candidateContract = IGenomeControl(_address);

        // NOTE: verify that a contract is what we expect - https://github.com/Lunyr/crowdsale-contracts/blob/cfadd15986c30521d8ba7d5b6f57b4fefcc7ac38/contracts/LunyrToken.sol#L117
        require(candidateContract.isGenomeControl());

        // Set the new contract address
        genomeControl = candidateContract;
        emit Mutation(_address);
        console.log("Mutation to %s", _address);
    }

    function _isReadyToBreed(Player memory _player) internal view returns (bool) {
        return (_player.pregnantWithId == 0) && (_player.cooldownEndBlock <= uint64(block.number));
    }

    function _isbreedingPermitted(uint256 _partnerAId, uint256 _partnerBId) internal view returns (bool) {
        address partnerAOwner = playerIndexToOwner[_partnerAId];
        address partnerBOwner = playerIndexToOwner[_partnerBId];

        return (partnerAOwner == partnerBOwner || partnerAllowedToAddress[_partnerBId] == partnerAOwner || partnerAllowedToAddress[_partnerAId] == partnerBOwner);
    }

    function _triggerCooldown(Player storage _player) internal {
        // Compute an estimation of the cooldown time in blocks (based on current cooldownIndex).
        _player.cooldownEndBlock = uint64((cooldowns[_player.cooldownIndex]/secondsPerBlock) + block.number);

        // Increment the breeding count, clamping it at 13, which is the length of the
        // cooldowns array. We could check the array size dynamically, but hard-coding
        // this as a constant saves gas. Yay, Solidity!
        if (_player.cooldownIndex < 13) {
            _player.cooldownIndex += 1;
        }
    }

    function _breedWith(uint256 _partnerAId, uint256 _partnerBId) internal {
        // Grab a reference to the Kitties from storage.
        Player storage partnerA = players[_partnerAId];
        Player storage partnerB = players[_partnerBId];

        // Mark the matron as pregnant, keeping track of who the sire is.
        partnerB.pregnantWithId = uint32(_partnerAId);

        partnerB.beB += 1;
        partnerA.beA += 1;

        // Trigger the cooldown for both parents.
        _triggerCooldown(partnerA);
        _triggerCooldown(partnerB);

        // Clear siring permission for both parents. This may not be strictly necessary
        // but it's likely to avoid confusion!
        delete partnerAllowedToAddress[_partnerAId];
        delete partnerAllowedToAddress[_partnerBId];

        // Every time a kitty gets pregnant, counter is incremented.
        pregnantPlayers++;

        // Emit the pregnancy event.
        emit Pregnant(playerIndexToOwner[_partnerBId], _partnerBId, _partnerAId, partnerB.cooldownEndBlock);
    }

    function breedWithAuto(uint256 _partnerAId, uint256 _partnerBId)
        external
        payable
        whenNotPaused
    {
        // Checks for payment.
        require(msg.value >= autoBirthFee);

        // Caller must own the matron.
        require(_owns(msg.sender, _partnerBId));

        // Neither sire nor matron are allowed to be on auction during a normal

        require(_isbreedingPermitted(_partnerAId, _partnerBId));

        // Grab a reference to the potential matron
        Player storage partnerB = players[_partnerBId];

        // Make sure matron isn't pregnant, or in the middle of a siring cooldown
        require(_isReadyToBreed(partnerB));

        // Grab a reference to the potential sire
        Player storage partnerA = players[_partnerAId];

        // Make sure sire isn't pregnant, or in the middle of a siring cooldown
        require(_isReadyToBreed(partnerA));

        // All checks passed, kitty gets pregnant!
        _breedWith(_partnerAId, _partnerBId);
    }

    function _isReadyToGiveBirth(Player memory _partnerB) private view returns (bool) {
        return (_partnerB.pregnantWithId != 0) && (_partnerB.cooldownEndBlock <= uint64(block.number));
    }

    function approvePartner(address _addr, uint256 _partnerId)
        external
        whenNotPaused
    {
        require(_owns(msg.sender, _partnerId));
        partnerAllowedToAddress[_partnerId] = _addr;
    }

    function isPregnant(uint256 _playerId)
        public
        view
        returns (bool)
    {
        require(_playerId > 0);
        return players[_playerId].pregnantWithId != 0;
    }

    function giveBirth(uint256 _partnerBId)
        external
        whenNotPaused
        returns(uint256)
    {
        // Grab a reference to the matron in storage.
        Player storage partnerB = players[_partnerBId];

        // Check that the matron is a valid cat.
        require(partnerB.birthTime != 0);

        // Check that the matron is pregnant, and that its time has come!
        require(_isReadyToGiveBirth(partnerB));

        // Grab a reference to the sire in storage.
        uint256 partnerAId = partnerB.pregnantWithId;
        Player storage partnerA = players[partnerAId];

        // Determine the higher generation number of the two parents
        uint16 parentGen = partnerB.generation;
        if (partnerA.generation > partnerB.generation) {
            parentGen = partnerA.generation;
        }

        // Call the sooper-sekret gene mixing operation.
        uint256 childChromosomeA = genomeControl.mixChromosome(partnerA.chromosomeA, partnerB.chromosomeA, partnerB.cooldownEndBlock - 1);
        uint256 childChromosomeB = genomeControl.mixChromosome(partnerA.chromosomeB, partnerB.chromosomeB, partnerB.cooldownEndBlock - 1);
        uint256 childChromosomeC = genomeControl.mixChromosome(partnerA.chromosomeC, partnerB.chromosomeC, partnerB.cooldownEndBlock - 1);
        
        // Make the new player!
        address owner = playerIndexToOwner[_partnerBId];
        uint256 playerId = _createPlayer(partnerB.pregnantWithId, _partnerBId , parentGen + 1, childChromosomeA, childChromosomeB, childChromosomeC, owner);

        // Clear the reference to sire from the matron (REQUIRED! Having pregnantWithId
        // set is what marks a matron as being pregnant.)
        delete partnerB.pregnantWithId;

        // Every time a kitty gives birth counter is decremented.
        pregnantPlayers--;

        // Send the balance fee to the person who made birth happen.
        // msg.sender.send(autoBirthFee);

        // return the new kitten's ID
        return playerId;
    }
}