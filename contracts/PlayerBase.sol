// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "./PlayerControl.sol";
import "hardhat/console.sol";

contract PlayerBase is PlayerControl , ERC721{

    event Birth(address owner, uint256 playerId, uint256 partnerAId, uint256 partnerBId, uint256 chromosomeA, uint256 chromosomeB, uint256 chromosomeC);

    // SaleClockAuction public saleAuction;

    // PartnerClockAuction public partnerAuction;

    /// @dev A mapping from cat IDs to the address that owns them. All cats have
    ///  some valid owner address, even gen0 cats are created with a non-zero owner.
    mapping (uint256 => address) public playerIndexToOwner;

    // @dev A mapping from owner address to count of tokens that address owns.
    //  Used internally inside balanceOf() to resolve ownership count.
    mapping (address => uint256) ownershipTokenCount;

    /// @dev A mapping from KittyIDs to an address that has been approved to call
    ///  transferFrom(). Each Kitty can only have one approved address for transfer
    ///  at any time. A zero value means no approval is outstanding.
    mapping (uint256 => address) public playerIndexToApproved;

    /// @dev A mapping from KittyIDs to an address that has been approved to use
    ///  this Kitty for siring via breedWith(). Each Kitty can only have one approved
    ///  address for siring at any time. A zero value means no approval is outstanding.
    mapping (uint256 => address) public partnerAllowedToAddress;

    struct Player {
        uint256 chromosomeA;
        uint256 chromosomeB;
        uint256 chromosomeC;
        uint64 birthTime;
        uint64 cooldownEndBlock;
        uint32 partnerAId;
        uint32 partnerBId;
        uint32 pregnantWithId;
        uint16 cooldownIndex;
        uint16 generation;
        uint16 beA;
        uint16 beB;
        uint256 currentWorld;
    }

    uint32[14] public cooldowns = [
        uint32(1 minutes),
        uint32(2 minutes),
        uint32(5 minutes),
        uint32(10 minutes),
        uint32(30 minutes),
        uint32(1 hours),
        uint32(2 hours),
        uint32(4 hours),
        uint32(8 hours),
        uint32(16 hours),
        uint32(1 days),
        uint32(2 days),
        uint32(4 days),
        uint32(7 days)
    ];

    uint256 public secondsPerBlock = 15;

    Player[] players;

    /// @notice Name and symbol of the non fungible token, as defined in ERC721.
    string public constant NAME = "CryptoPlayer";
    string public constant SYMBOL = "CA";

    constructor()
        ERC721(NAME, SYMBOL){}

    function _approvedFor(address _claimant, uint256 _tokenId) internal view returns (bool) {
        return playerIndexToApproved[_tokenId] == _claimant;
    }

    function transferFrom(
        address _from,
        address _to,
        uint256 _tokenId
    )
        override
        public
        whenNotPaused
    {
        require(_to != address(0));
        require(_to != address(this));
        require(_approvedFor(msg.sender, _tokenId));
        require(_owns(_from, _tokenId));

        _transfer(_from, _to, _tokenId);
    }

    function _approve(uint256 _tokenId, address _approved) internal {
        playerIndexToApproved[_tokenId] = _approved;
    }

    function approve(
        address _to,
        uint256 _tokenId
    )
        override
        public
        whenNotPaused
    {
        // Only an owner can grant transfer approval.
        require(_owns(msg.sender, _tokenId));

        // Register the approval (replacing any previous approval).
        _approve(_tokenId, _to);

        // Emit approval event.
        emit Approval(msg.sender, _to, _tokenId);
    }

    function _transfer(address _from, address _to, uint256 _tokenId) override internal {
        // Since the number of kittens is capped to 2^32 we can't overflow this
        ownershipTokenCount[_to]++;
        // transfer ownership
        playerIndexToOwner[_tokenId] = _to;
        // When creating new kittens _from is 0x0, but we can't account that address.
        if (_from != address(0)) {
            ownershipTokenCount[_from]--;
            // once the kitten is transferred also clear sire allowances
            delete partnerAllowedToAddress[_tokenId];
            // clear any previously approved ownership exchange
            delete playerIndexToApproved[_tokenId];
        }
        // Emit the transfer event.
        emit Transfer(_from, _to, _tokenId);
    }

    function transfer(
        address _to,
        uint256 _tokenId
    )
        external
        whenNotPaused
    {
        require(_to != address(0));
        require(_to != address(this));
        require(_owns(msg.sender, _tokenId));
        _transfer(msg.sender, _to, _tokenId);
    }

    function totalSupply() public view returns (uint) {
        return players.length - 1;
    }

    function tokensOfOwner(address _owner) external view returns(uint256[] memory ownerTokens) {
        uint256 tokenCount = balanceOf(_owner);

        if (tokenCount == 0) {
            return new uint256[](0);
        } else {
            uint256[] memory result = new uint256[](tokenCount);
            uint256 totalPlayers = totalSupply();
            uint256 resultIndex = 0;

            uint256 playerId;

            for (playerId = 1; playerId <= totalPlayers; playerId++) {
                if (playerIndexToOwner[playerId] == _owner) {
                    result[resultIndex] = playerId;
                    resultIndex++;
                }
            }

            return result;
        }
    }

    function ownerOf(uint256 _tokenId)
        public
        view
        override
        returns (address owner)
    {
        owner = playerIndexToOwner[_tokenId];

        require(owner != address(0));
    }

    function _owns(address _claimant, uint256 _tokenId) internal view returns (bool) {
        return playerIndexToOwner[_tokenId] == _claimant;
    }

    function balanceOf(address _owner) override public view returns (uint256 count) {
        return ownershipTokenCount[_owner];
    }

    function setSecondsPerBlock(uint256 secs) external onlyControlLevel {
        require(secs < cooldowns[0]);
        secondsPerBlock = secs;
    }

    function _createPlayer(
        uint256 _partnerAId,
        uint256 _partnerBId,
        uint256 _generation,
        uint256 _chromosomeA,
        uint256 _chromosomeB,
        uint256 _chromosomeC,
        address _owner
    )
        internal
        returns (uint)
    {
        require(_partnerAId == uint256(uint32(_partnerAId)));
        require(_partnerBId == uint256(uint32(_partnerBId)));
        require(_generation == uint256(uint16(_generation)));

        // New kitty starts with the same cooldown as parent gen/2
        uint16 cooldownIndex = uint16(_generation / 2);
        if (cooldownIndex > 13) {
            cooldownIndex = 13;
        }

        Player memory _player = Player({
            chromosomeA: _chromosomeA,
            chromosomeB: _chromosomeB,
            chromosomeC: _chromosomeC,
            birthTime: uint64(block.timestamp),
            cooldownEndBlock: 0,
            partnerAId: uint32(_partnerAId),
            partnerBId: uint32(_partnerBId),
            pregnantWithId: 0,
            cooldownIndex: cooldownIndex,
            generation: uint16(_generation),
            beA: 0,
            beB: 0,
            currentWorld:0
        });

        players.push(_player);
        uint256 newPlayerId = players.length - 1;

        require(newPlayerId == uint256(uint32(newPlayerId)));

        // emit the birth event
        emit Birth(
            _owner,
            newPlayerId,
            uint256(_player.partnerAId),
            uint256(_player.partnerBId),
            _player.chromosomeA,
            _player.chromosomeB,
            _player.chromosomeC
        );

        // This will assign ownership, and also emit the Transfer event as
        // per ERC721 draft
        _transfer(address(0), _owner, newPlayerId);
        return newPlayerId;
    }
}