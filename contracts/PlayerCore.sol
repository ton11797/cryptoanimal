//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;
import "./PlayerMinting.sol";

/// @title A facet of PlayerCore that manages special access privileges.
/// @author Denice
/// @dev See the PlayerCore contract documentation to understand how the various contract facets are arranged.
contract PlayerCore is PlayerMinting {

    uint256 MAX_INT = 0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff;

    /// @notice Creates the main CryptoKitties smart contract instance.
    constructor(){
        paused = true;
        
        bossAddress = msg.sender;

        adminAddress = msg.sender;


        _createPlayer(0, 0, 0, MAX_INT, MAX_INT, MAX_INT, address(0));
    }

    receive() external payable {
    }

    function getPlayer(uint256 _id)
        external
        view
        returns (
        bool isGestating,
        bool isReady,
        uint256 cooldownIndex,
        uint256 nextActionAt,
        uint256 pregnantWithId,
        uint256 birthTime,
        uint256 partnerAId,
        uint256 partnerBId,
        uint256 generation,
        uint256 chromosomeA,
        uint256 chromosomeB,
        uint256 chromosomeC
    ) {
        Player storage player = players[_id];

        // if this variable is 0 then it's not gestating
        isGestating = (player.pregnantWithId != 0);
        isReady = (player.cooldownEndBlock <= block.number);
        cooldownIndex = uint256(player.cooldownIndex);
        nextActionAt = uint256(player.cooldownEndBlock);
        pregnantWithId = uint256(player.pregnantWithId);
        birthTime = uint256(player.birthTime);
        partnerAId = uint256(player.partnerAId);
        partnerBId = uint256(player.partnerBId);
        generation = uint256(player.generation);
        chromosomeA = player.chromosomeA;
        chromosomeB = player.chromosomeB;
        chromosomeC = player.chromosomeC;
    }

    function withdrawBalance() external onlyAdmin {
        uint256 balance = address(this).balance;
        // Subtract all the currently pregnant kittens we have, plus 1 of margin.
        uint256 subtractFees = (pregnantPlayers + 1) * autoBirthFee;

        if (balance > subtractFees) {
            payable(adminAddress).transfer(balance - subtractFees);
        }
    }
}