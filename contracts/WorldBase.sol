//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;
import "hardhat/console.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "./WorldControl.sol";
import "./interface/IPlayerBase.sol";

contract WorldBase is WorldControl, ERC721{
    event Birth(address owner, uint256 worldId);

    // SaleClockAuction public saleAuction;

    // PartnerClockAuction public partnerAuction;

    /// @dev A mapping from cat IDs to the address that owns them. All cats have
    ///  some valid owner address, even gen0 cats are created with a non-zero owner.
    mapping (uint256 => address) public worldIndexToOwner;

    // @dev A mapping from owner address to count of tokens that address owns.
    //  Used internally inside balanceOf() to resolve ownership count.
    mapping (address => uint256) ownershipTokenCount;

    /// @dev A mapping from KittyIDs to an address that has been approved to call
    ///  transferFrom(). Each Kitty can only have one approved address for transfer
    ///  at any time. A zero value means no approval is outstanding.
    mapping (uint256 => address) public worldIndexToApproved;

    /// @dev A mapping from KittyIDs to an address that has been approved to use
    ///  this Kitty for siring via breedWith(). Each Kitty can only have one approved
    ///  address for siring at any time. A zero value means no approval is outstanding.
    mapping (uint256 => address) public partnerAllowedToAddress;

    struct World {
        string worldName;
        uint64 birthTime;
        uint256 populationCount;
        uint256 worldSize;
        uint256[] population;
        bool isBorderClose;
    }

    World[] worlds;

    IPlayerBase public playerBaseAddress;

    /// @notice Name and symbol of the non fungible token, as defined in ERC721.
    string public constant NAME = "CryptoWorld";
    string public constant SYMBOL = "CW";

    constructor()
        ERC721(NAME, SYMBOL){}

    function _approvedFor(address _claimant, uint256 _tokenId) internal view returns (bool) {
        return worldIndexToApproved[_tokenId] == _claimant;
    }

    function transferFrom(
        address _from,
        address _to,
        uint256 _tokenId
    )
        override
        public
        whenNotPaused
    {
        require(_to != address(0));
        require(_to != address(this));
        require(_approvedFor(msg.sender, _tokenId));
        require(_owns(_from, _tokenId));

        _transfer(_from, _to, _tokenId);
    }

    function _approve(uint256 _tokenId, address _approved) internal {
        worldIndexToApproved[_tokenId] = _approved;
    }

    function approve(
        address _to,
        uint256 _tokenId
    )
        override
        public
        whenNotPaused
    {
        // Only an owner can grant transfer approval.
        require(_owns(msg.sender, _tokenId));

        // Register the approval (replacing any previous approval).
        _approve(_tokenId, _to);

        // Emit approval event.
        emit Approval(msg.sender, _to, _tokenId);
    }

    function _transfer(address _from, address _to, uint256 _tokenId) override internal {
        // Since the number of kittens is capped to 2^32 we can't overflow this
        ownershipTokenCount[_to]++;
        // transfer ownership
        worldIndexToOwner[_tokenId] = _to;
        // When creating new kittens _from is 0x0, but we can't account that address.
        if (_from != address(0)) {
            ownershipTokenCount[_from]--;
            // once the kitten is transferred also clear sire allowances
            delete partnerAllowedToAddress[_tokenId];
            // clear any previously approved ownership exchange
            delete worldIndexToApproved[_tokenId];
        }
        // Emit the transfer event.
        emit Transfer(_from, _to, _tokenId);
    }

    function transfer(
        address _to,
        uint256 _tokenId
    )
        external
        whenNotPaused
    {
        require(_to != address(0));
        require(_to != address(this));
        require(_owns(msg.sender, _tokenId));
        _transfer(msg.sender, _to, _tokenId);
    }

    function totalSupply() public view returns (uint) {
        return worlds.length - 1;
    }

    function tokensOfOwner(address _owner) external view returns(uint256[] memory ownerTokens) {
        uint256 tokenCount = balanceOf(_owner);

        if (tokenCount == 0) {
            return new uint256[](0);
        } else {
            uint256[] memory result = new uint256[](tokenCount);
            uint256 totalWorlds = totalSupply();
            uint256 resultIndex = 0;

            uint256 worldId;

            for (worldId = 1; worldId <= totalWorlds; worldId++) {
                if (worldIndexToOwner[worldId] == _owner) {
                    result[resultIndex] = worldId;
                    resultIndex++;
                }
            }

            return result;
        }
    }

    function ownerOf(uint256 _tokenId)
        public
        view
        override
        returns (address owner)
    {
        owner = worldIndexToOwner[_tokenId];

        require(owner != address(0));
    }

    function _owns(address _claimant, uint256 _tokenId) internal view returns (bool) {
        return worldIndexToOwner[_tokenId] == _claimant;
    }

    function balanceOf(address _owner) override public view returns (uint256 count) {
        return ownershipTokenCount[_owner];
    }

    function _createWorld(
        string memory worldName,
        uint256 worldSize,
        address _owner
    )
        internal
        returns (uint)
    {

        World memory _world = World({
            worldName: worldName,
            birthTime: uint64(block.timestamp),
            populationCount:0,
            worldSize: worldSize,
            population: new uint256[](worldSize),
            isBorderClose: true
        });

        worlds.push(_world);
        uint256 newWorldId = worlds.length - 1;

        require(newWorldId == uint256(uint32(newWorldId)));

        // emit the birth event
        emit Birth(
            _owner,
            newWorldId
        );

        // This will assign ownership, and also emit the Transfer event as
        // per ERC721 draft
        _transfer(address(0), _owner, newWorldId);
        return newWorldId;
    }
}