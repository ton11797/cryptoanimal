//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

/// @title A facet of PlayerCore that manages special access privileges.
/// @author Denice
/// @dev See the PlayerCore contract documentation to understand how the various contract facets are arranged.
contract WorldControl {

    /// @dev Emited when contract is upgraded - See README.md for updgrade plan
    event ContractUpgrade(address newContract);

    // The addresses of the accounts (or contracts) that can execute actions within each roles.
    address public bossAddress;
    address public adminAddress;

    // @dev Keeps track whether the contract is paused. When that is true, most actions are blocked
    bool public paused = false;

    /// @dev Access modifier for Boss-only functionality
    modifier onlyBoss() {
        require(msg.sender == bossAddress,"OB");
        _;
    }

    /// @dev Access modifier for Admin-only functionality
    modifier onlyAdmin() {
        require(msg.sender == adminAddress,"OA");
        _;
    }

    modifier onlyControlLevel() {
        require(
            msg.sender == bossAddress ||
            msg.sender == adminAddress
            ,"OC"
        );
        _;
    }

    /// @dev Assigns a new address to act as the Boss. Only available to the current Boss.
    /// @param _newBoss The address of the new Boss
    function setBoss(address _newBoss) external onlyBoss {
        require(_newBoss != address(0),"A0");

        bossAddress = _newBoss;
    }

    /// @dev Assigns a new address to act as the Admin. Only available to the current Boss.
    /// @param _newAdmin The address of the new Admin
    function setAdmin(address _newAdmin) external onlyBoss {
        require(_newAdmin != address(0),"A0");

        adminAddress = _newAdmin;
    }

    /*** Pausable functionality adapted from OpenZeppelin ***/

    /// @dev Modifier to allow actions only when the contract IS NOT paused
    modifier whenNotPaused() {
        require(!paused,"can't cell when contract paused");
        _;
    }

    /// @dev Modifier to allow actions only when the contract IS paused
    modifier whenPaused {
        require(paused,"can't cell when contract not paused");
        _;
    }

    /// @dev Called by any "Control-level" role to pause the contract. Used only when
    ///  a bug or exploit is detected and we need to limit damage.
    function pause() external onlyControlLevel whenNotPaused {
        paused = true;
    }

    /// @dev Unpauses the smart contract. Can only be called by the Boss, since
    ///  one reason we may pause the contract is when admin accounts are
    ///  compromised.
    /// @notice This is public rather than external so it can be called by
    ///  derived contracts.
    function unpause() public onlyBoss whenPaused {
        // can't unpause if contract was upgraded
        paused = false;
    }
}