//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "hardhat/console.sol";

contract GenomeControl{

    uint256 internal constant maskLast8Bits = uint256(0xff);
    uint256 internal constant maskFirst248Bits = uint256(0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff00);

    bool public isGenomeControl = true;
    /// @dev the function as defined in the breeding contract - as defined in CK bible
    function mixChromosome(uint256 _chromosomeA, uint256 _chromosomeB, uint256 _targetBlock) public returns (uint256) {
        return _chromosomeA;
    }

    function _sliceNumber(uint256 _n, uint256 _nbits, uint256 _offset) private pure returns (uint256) {
        // mask is made by shifting left an offset number of times
        uint256 mask = uint256((2**_nbits) - 1) << _offset;
        // AND n with mask, and trim to max of _nbits bits
        return uint256((_n & mask) >> _offset);
    }

    function _get4Bits(uint256 _input, uint256 _slot) internal pure returns(uint8) {
        return uint8(_sliceNumber(_input, uint256(4), _slot * 4));
    }

    function rand(uint256 _chromosomeA, uint256 _chromosomeB,uint256 _targetBlock) public view returns(uint8[6] memory){
        uint8[6] memory randNum;
        bool[6] memory stopper;
        uint256 randomN = uint256(blockhash(_targetBlock));
        
        if (randomN == 0) {
            _targetBlock = (block.number & maskFirst248Bits) + (_targetBlock & maskLast8Bits);
            if (_targetBlock >= block.number) _targetBlock -= 256;
            randomN = uint256(blockhash(_targetBlock));
        }

        randomN =  uint256(keccak256(abi.encodePacked(block.timestamp, randomN, _chromosomeA, _chromosomeB, _targetBlock)));

        for(uint i=0;i<6;i++){
            randNum[i] = 0;
            stopper[i] = true;
        }

        uint toggleCheck = 0;

        for(uint i=0;i<64;i++){
            uint currentByteMod2 = (_get4Bits(randomN,i))%2;
            if(stopper[0]){
                if(currentByteMod2 == 0){
                    randNum[0] += 1;
                }else{
                    stopper[0] = false;
                }
            }

            if(stopper[1]){
                if(currentByteMod2 == 1){
                    randNum[1] += 1;
                }else{
                    stopper[1] = false;
                }
            }

            if(stopper[2]){
                if(currentByteMod2 == toggleCheck){
                    randNum[2] += 1;
                    if(toggleCheck == 1){
                        toggleCheck = 0;
                    }else{
                        toggleCheck = 1;
                    }
                }else{
                    stopper[2] = false;
                }
            }
            
            if(i>31){

                if(stopper[3]){
                    if(currentByteMod2 == 0){
                        randNum[3] += 1;
                    }else{
                        stopper[3] = false;
                    }
                }

                if(stopper[4]){
                    if(currentByteMod2 == 1){
                        randNum[4] += 1;
                    }else{
                        stopper[4] = false;
                    }
                }

                if(stopper[5]){
                if(currentByteMod2 == toggleCheck){
                    randNum[5] += 1;
                    if(toggleCheck == 1){
                        toggleCheck = 0;
                    }else{
                        toggleCheck = 1;
                    }
                }else{
                    stopper[5] = false;
                }
            }
            }
        }
        return randNum;
    }
}