//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

contract Genome{
    /// @dev the function as defined in the breeding contract - as defined in CK bible
    function mixGenes(uint256 _genes1, uint256 _genes2, uint256 _targetBlock) public returns (uint256) {
        return _genes1;
    }
}