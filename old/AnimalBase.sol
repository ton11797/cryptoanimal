//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;
import {PlayerControl} from "./PlayerControl.sol";

contract PlayerBase is PlayerControl {
    /*** EVENTS ***/

    /// @dev The Birth event is fired whenever a new player comes into existence. This obviously
    ///  includes any time a player is created through the giveBirth method, but it is also called
    ///  when a new gen0 player is created.
    event Birth(address owner, uint256 playerId, uint256 partnerAId, uint256 partnerBId, uint256 genome);

    struct Player {
        // The Player's genetic code is packed into these 256-bits
        uint256 genes;

        // The timestamp from the block when this player came into existence.
        uint64 birthTime;

        // The minimum timestamp after which this player can engage in breeding
        // activities again. This same timestamp is used for the pregnancy
        // timer (for partnerB) as well as the partnerA cooldown.
        uint64 cooldownEndBlock;

        // The ID of the parents of this player, set to 0 for gen0 player.
        // Note that using 32-bit unsigned integers limits us to a "mere"
        // 4 billion player. This number might seem small until you realize
        // that Ethereum currently has a limit of about 500 million
        // transactions per year! So, this definitely won't be a problem
        // for several years (even as Ethereum learns to scale).
        uint32 partnerBId;
        uint32 partnerAId;

        // Set to the ID of the sire player for partnerB that are pregnant,
        // zero otherwise. A non-zero value here is how we know a player
        // is pregnant. Used to retrieve the genetic material for the new
        // player when the birth transpires.
        uint32 partnerAWithId;

        // Set to the index in the cooldown array (see below) that represents
        // the current cooldown duration for this player. This starts at zero
        // for gen0 cats, and is initialized to floor(generation/2) for others.
        // Incremented by one for each successful breeding action, regardless
        // of whether this cat is acting as matron or sire.
        uint16 cooldownIndex;

        // The "generation number" of this player. Players minted by the CK contract
        // for sale are called "gen0" and have a generation number of 0. The
        // generation number of all other players is the larger of the two generation
        // numbers of their parents, plus one.
        // (i.e. max(partnerA.generation, partnerB.generation) + 1)
        uint16 generation;
    }

        /*** CONSTANTS ***/

    /// @dev A lookup table indicating the cooldown duration after any successful
    ///  breeding action, called "pregnancy time" for matrons and "siring cooldown"
    ///  for sires. Designed such that the cooldown roughly doubles each time a cat
    ///  is bred, encouraging owners not to just keep breeding the same cat over
    ///  and over again. Caps out at one week (a cat can breed an unbounded number
    ///  of times, and the maximum cooldown is always seven days).
    uint32[14] public cooldowns = [
        uint32(1 minutes),
        uint32(2 minutes),
        uint32(5 minutes),
        uint32(10 minutes),
        uint32(30 minutes),
        uint32(1 hours),
        uint32(2 hours),
        uint32(4 hours),
        uint32(8 hours),
        uint32(16 hours),
        uint32(1 days),
        uint32(2 days),
        uint32(4 days),
        uint32(7 days)
    ];

    // An approximation of currently how many seconds are in between blocks.
    uint256 public secondsPerBlock = 15;

    /*** STORAGE ***/

    /// @dev An array containing the player struct for all Kitties in existence. The ID
    ///  of each cat is actually an index into this array. Note that ID 0 is a negacat,
    ///  the unplayer, the mythical beast that is the parent of all gen0 cats. A bizarre
    ///  creature that is both matron and sire... to itself! Has an invalid genetic code.
    ///  In other words, cat ID 0 is invalid... ;-)
    Player[] players;

    /// @dev A mapping from cat IDs to the address that owns them. All cats have
    ///  some valid owner address, even gen0 cats are created with a non-zero owner.
    mapping (uint256 => address) public playerIndexToOwner;

    // @dev A mapping from owner address to count of tokens that address owns.
    //  Used internally inside balanceOf() to resolve ownership count.
    mapping (address => uint256) public ownershipTokenCount;

    /// @dev A mapping from playerIDs to an address that has been approved to call
    ///  transferFrom(). Each player can only have one approved address for transfer
    ///  at any time. A zero value means no approval is outstanding.
    mapping (uint256 => address) public playerIndexToApproved;

    /// @dev A mapping from playerIDs to an address that has been approved to use
    ///  this player for siring via breedWith(). Each player can only have one approved
    ///  address for siring at any time. A zero value means no approval is outstanding.
    mapping (uint256 => address) public partnerBAllowedToAddress;

    // /// @dev The address of the ClockAuction contract that handles sales of Kitties. This
    // ///  same contract handles both peer-to-peer sales as well as the gen0 sales which are
    // ///  initiated every 15 minutes.
    // SaleClockAuction public saleAuction;

    // /// @dev The address of a custom ClockAuction subclassed contract that handles siring
    // ///  auctions. Needs to be separate from saleAuction because the actions taken on success
    // ///  after a sales and siring auction are quite different.
    // SiringClockAuction public siringAuction;

    /// @dev Assigns ownership of a specific player to an address.
    function _transfer(address _from, address _to, uint256 _tokenId) internal {
        // Since the number of player is capped to 2^32 we can't overflow this
        ownershipTokenCount[_to]++;
        // transfer ownership
        playerIndexToOwner[_tokenId] = _to;
        // When creating new player _from is 0x0, but we can't account that address.
        if (_from != address(0)) {
            ownershipTokenCount[_from]--;
            // once the kitten is transferred also clear partnerB allowances
            delete partnerBAllowedToAddress[_tokenId];
            // clear any previously approved ownership exchange
            delete playerIndexToApproved[_tokenId];
        }
    }

    /// @dev An internal method that creates a new player and stores it. This
    ///  method doesn't do any checking and should only be called when the
    ///  input data is known to be valid. Will generate both a Birth event
    ///  and a Transfer event.
    /// @param _partnerAId The player ID of the partnerA of this cat (zero for gen0)
    /// @param _partnerBId The player ID of the partnerB of this cat (zero for gen0)
    /// @param _generation The generation number of this player, must be computed by caller.
    /// @param _genes The player's genetic code.
    /// @param _owner The inital owner of this player, must be non-zero (except for the unplayer, ID 0)
    function _createPlayer(
        uint256 _partnerAId,
        uint256 _partnerBId,
        uint256 _generation,
        uint256 _genes,
        address _owner
    )
        internal
        returns (uint)
    {
        // These requires are not strictly necessary, our calling code should make
        // sure that these conditions are never broken. However! _createplayer() is already
        // an expensive call (for storage), and it doesn't hurt to be especially careful
        // to ensure our data structures are always valid.
        require(_partnerAId == uint256(uint32(_partnerAId)),"invalid _partnerAId");
        require(_partnerBId == uint256(uint32(_partnerBId)),"invalid _partnerBId");
        require(_generation == uint256(uint16(_generation)),"invalid _generation");

        // New player starts with the same cooldown as parent gen/2
        uint16 cooldownIndex = uint16(_generation / 2);
        if (cooldownIndex > 13) {
            cooldownIndex = 13;
        }

        Player memory _player = Player({
            genes: _genes,
            birthTime: uint64(block.timestamp),
            cooldownEndBlock: 0,
            partnerAId: uint32(_partnerAId),
            partnerBId: uint32(_partnerBId),
            partnerAWithId: 0,
            cooldownIndex: cooldownIndex,
            generation: uint16(_generation)
        });

        players.push(_player);
        uint256 newPlayerId = players.length-1;

        // It's probably never going to happen, 4 billion player is A LOT, but
        // let's just be 100% sure we never let this happen.
        require(newPlayerId == uint256(uint32(newPlayerId)),"newPlayerId overflow");

        // emit the birth event
        emit Birth(
            _owner,
            newPlayerId,
            uint256(_player.partnerAId),
            uint256(_player.partnerBId),
            _player.genes
        );

        // This will assign ownership, and also emit the Transfer event as
        // per ERC721 draft
        _transfer(address(0), _owner, newPlayerId);

        return newPlayerId;
    }

    // Any Control level can fix how many seconds per blocks are currently observed.
    function setSecondsPerBlock(uint256 secs) external onlyControlLevel {
        require(secs < cooldowns[0],"secs > 60 sec");
        secondsPerBlock = secs;
    }
}