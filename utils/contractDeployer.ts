import hre, {deployments, getNamedAccounts} from 'hardhat';
import {PlayerCore} from '../typechain/PlayerCore';
import {GenomeControl} from '../typechain/GenomeControl';
import {ContractId, ContractType} from '../types';
import {getContractAt} from './contractGetter';
import registerContractInJsonDb from './registerContractInJsonDb';

const {deploy} = deployments;

export const deployGenome = async (name = ''): Promise<GenomeControl> => {
  const {deployer} = await getNamedAccounts();
  const contract = ContractId.GenomeControl;
  const contractName = `${ContractId.GenomeControl}${name || ''}`;
  const result = await deploy(contractName, {
    from: deployer,
    contract,
  });
  console.log(`${contract}: ${result.address}`);

  await registerContractInJsonDb(ContractType.Contract, contract, hre.network.name, result);

  return await getContractAt(contract, result.address);
};

export const deployPlayer = async (): Promise<PlayerCore> => {
  const {deployer} = await getNamedAccounts();
  const contract = ContractId.PlayerCore;
  const result = await deploy(contract, {
    from: deployer,
    contract,
  });
  console.log(`${contract}: ${result.address}`);

  await registerContractInJsonDb(ContractType.Contract, contract, hre.network.name, result);

  return await getContractAt(contract, result.address);
};
