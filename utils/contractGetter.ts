import {Contract} from 'ethers';
import {ContractId} from '../types';
import hre from 'hardhat';

export const getContractAt = async <T extends Contract>(contractId: ContractId, address: string): Promise<T> =>
  (await hre.ethers.getContractAt(contractId, address)) as T;
