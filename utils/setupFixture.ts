import {BigNumber} from 'ethers';
import {deployments, ethers, getNamedAccounts} from 'hardhat';
import {PlayerCore} from '../typechain/PlayerCore';
import {ContractId, Fixture, User} from '../types';
import {createPromoPlayer} from './protocolTx';
export const setupFixture = deployments.createFixture(async (hre) => {
  await deployments.fixture(['testEnv']);

  const {deployer, boss, admin, user1, user2, user3, user4, user5} = await getNamedAccounts();

  const addressPlayerCore = (await ethers.getContract(ContractId.PlayerCore)) as PlayerCore;
  await addressPlayerCore.connect(await ethers.getSigner(boss)).setAdmin(admin);
  await addressPlayerCore.connect(await ethers.getSigner(boss)).unpause();

  return {
    deployer: await setupUser(deployer, 'deployer', admin),
    boss: await setupUser(boss, 'boss', admin),
    admin: await setupUser(admin, 'admin', admin),
    user1: await setupUser(user1, 'user1', admin),
    user2: await setupUser(user2, 'user2', admin),
    user3: await setupUser(user3, 'user3', admin),
    user4: await setupUser(user4, 'user4', admin),
    user5: await setupUser(user5, 'user5', admin, false),
  } as unknown as Fixture;
});

async function setupUser(address: string, name: string, adminAddress: string, genPlayer = true): Promise<User> {
  const signer = await ethers.getSigner(address);
  const ownPlayerIds: string[] = [];
  if (genPlayer) {
    const addressPlayerCore = (await ethers.getContract(ContractId.PlayerCore)) as PlayerCore;
    const genome = 0 as unknown as BigNumber;
    ownPlayerIds.push(
      await createPromoPlayer({address: adminAddress} as User, {address: address} as User, genome, genome, genome)
    );
    ownPlayerIds.push(
      await createPromoPlayer({address: adminAddress} as User, {address: address} as User, genome, genome, genome)
    );
  }
  const user: any = {address, name: name ? name : address, signer, ownPlayerIds} as User;
  return user;
}
