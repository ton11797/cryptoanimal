import {ContractReceipt, ContractTransaction} from 'ethers';
import {ethers} from 'hardhat';
export const waitForTx = async (tx: ContractTransaction): Promise<ContractReceipt> => await tx.wait(1);

export const advanceTimeAndBlock = async function (seconds: number): Promise<void> {
  const currentBlockNumber = await ethers.provider.getBlockNumber();
  const currentBlock = await ethers.provider.getBlock(currentBlockNumber);

  if (currentBlock === null) {
    /* Workaround for https://github.com/nomiclabs/hardhat/issues/1183
     */
    await ethers.provider.send('evm_increaseTime', [seconds]);
    await ethers.provider.send('evm_mine', []);
    //Set the next blocktime back to 15 seconds
    await ethers.provider.send('evm_increaseTime', [15]);
    return;
  }
  const currentTime = currentBlock.timestamp;
  const futureTime = currentTime + seconds;
  await ethers.provider.send('evm_setNextBlockTimestamp', [futureTime]);
  await ethers.provider.send('evm_mine', []);
};

export const advanceBlock = async (timestamp: number): Promise<void> => {
  await ethers.provider.send('evm_mine', [timestamp]);
};

export const advanceNBlock = async (n: number): Promise<void> => {
  for (let i = 0; i < n; i++) {
    await advanceBlock(Date.now() + i);
  }
};
