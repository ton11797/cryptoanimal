import {User} from '../types';

export const FINNEY = 1e15;
export const ADDRESS_ZERRO = '0x0000000000000000000000000000000000000000';
export const USER_ZERRO = {address: ADDRESS_ZERRO} as User;
