import {expect} from 'chai';
import {BigNumber, ContractReceipt} from 'ethers';
import {deployments, ethers, getNamedAccounts} from 'hardhat';
import {PlayerCore} from '../typechain/PlayerCore';
import {GenomeControl} from '../typechain/GenomeControl';
import {PlayerData, ContractId, ExpectedTxResult, User} from '../types';
import {FINNEY} from './constant';
import {waitForTx} from './hhNetwork';

export const getGenomeControl = async (): Promise<string> => {
  const addressPlayerCore = (await ethers.getContract(ContractId.PlayerCore)) as PlayerCore;
  return await addressPlayerCore.genomeControl();
};

export const setGenomeControl = async (
  sender: User,
  genomeControl: GenomeControl,
  option?: {
    expectedResult?: ExpectedTxResult;
  }
): Promise<ContractReceipt> => {
  const {expectedResult} = option || {};
  const addressPlayerCore = (await ethers.getContract(ContractId.PlayerCore)) as PlayerCore;
  if (expectedResult?.revertMessage) {
    await expect(
      addressPlayerCore.connect(await ethers.getSigner(sender.address)).setGenomeControlAddress(genomeControl.address)
    ).to.be.revertedWith(expectedResult.revertMessage);
    return {} as ContractReceipt;
  }

  const txResult = await waitForTx(
    await addressPlayerCore
      .connect(await ethers.getSigner(sender.address))
      .setGenomeControlAddress(genomeControl.address)
  );
  return txResult;
};

export const getPlayer = async (sender: User, id: string): Promise<PlayerData> => {
  const addressPlayerCore = (await ethers.getContract(ContractId.PlayerCore)) as PlayerCore;
  const result = await addressPlayerCore.connect(await ethers.getSigner(sender.address)).getPlayer(id);
  return result as unknown as PlayerData;
};

export const createPromoPlayer = async (
  sender: User,
  to: User,
  chromosomeA: BigNumber,
  chromosomeB: BigNumber,
  chromosomeC: BigNumber,
  option?: {
    expectedResult?: ExpectedTxResult;
  }
) => {
  const addressPlayerCore = (await ethers.getContract(ContractId.PlayerCore)) as PlayerCore;
  const {expectedResult} = option || {};

  if (expectedResult?.revertMessage) {
    await expect(
      addressPlayerCore
        .connect(await ethers.getSigner(sender.address))
        .createPromoPlayer(chromosomeA, chromosomeB, chromosomeC, to.address)
    ).to.be.revertedWith(expectedResult.revertMessage);
    return {} as ContractReceipt;
  }
  const txResult = await waitForTx(
    await addressPlayerCore
      .connect(await ethers.getSigner(sender.address))
      .createPromoPlayer(chromosomeA, chromosomeB, chromosomeC, to.address)
  );
  const eventBirth = txResult.events?.find((el) => el.event === 'Birth');
  if (eventBirth && eventBirth.args && eventBirth.args?.length > 0) {
    return eventBirth.args[1];
  }
  return null;
};

export const withdrawBalance = async (
  sender: User,
  option?: {
    expectedResult?: ExpectedTxResult;
  }
) => {
  const addressPlayerCore = (await ethers.getContract(ContractId.PlayerCore)) as PlayerCore;
  const {expectedResult} = option || {};

  await addressPlayerCore.connect(await ethers.getSigner(sender.address)).withdrawBalance();
};

export const breedWithAuto = async (
  sender: User,
  _partnerAId: string,
  _partnerBId: string,
  fee: BigNumber,
  option?: {
    expectedResult?: ExpectedTxResult;
  }
) => {
  const addressPlayerCore = (await ethers.getContract(ContractId.PlayerCore)) as PlayerCore;
  const {expectedResult} = option || {};
  await addressPlayerCore
    .connect(await ethers.getSigner(sender.address))
    .breedWithAuto(_partnerAId, _partnerBId, {value: fee});
};

export const giveBirth = async (
  sender: User,
  _partnerBId: string,
  option?: {
    expectedResult?: ExpectedTxResult;
  }
): Promise<string> => {
  const addressPlayerCore = (await ethers.getContract(ContractId.PlayerCore)) as PlayerCore;
  const {expectedResult} = option || {};
  const txResult = await waitForTx(
    await addressPlayerCore.connect(await ethers.getSigner(sender.address)).giveBirth(_partnerBId)
  );
  const eventBirth = txResult.events?.find((el) => el.event === 'Birth');
  if (eventBirth && eventBirth.args && eventBirth.args?.length > 0) {
    return eventBirth.args[1];
  }
  return '';
};

export const balanceOf = async (sender: User, user: User) => {
  const addressPlayerCore = (await ethers.getContract(ContractId.PlayerCore)) as PlayerCore;
  return await addressPlayerCore.connect(await ethers.getSigner(sender.address)).balanceOf(user.address);
};

export const transfer = async (sender: User, to: User, playerId: string) => {
  const addressPlayerCore = (await ethers.getContract(ContractId.PlayerCore)) as PlayerCore;
  return await addressPlayerCore.connect(await ethers.getSigner(sender.address)).transfer(to.address, playerId);
};

export const ownerOf = async (sender: User, token: string) => {
  const addressPlayerCore = (await ethers.getContract(ContractId.PlayerCore)) as PlayerCore;
  return await addressPlayerCore.connect(await ethers.getSigner(sender.address)).ownerOf(token);
};

export const tokensOfOwner = async (sender: User, user: User) => {
  const addressPlayerCore = (await ethers.getContract(ContractId.PlayerCore)) as PlayerCore;
  return await addressPlayerCore.connect(await ethers.getSigner(sender.address)).tokensOfOwner(user.address);
};

export const rand = async (sender: User, genome: BigNumber, genome2: BigNumber, tragetBlock: BigNumber) => {
  const addressGenomeControl = (await ethers.getContract(ContractId.GenomeControl)) as GenomeControl;
  return await addressGenomeControl.connect(await ethers.getSigner(sender.address)).rand(genome, genome2, tragetBlock);
};

export const setSecondsPerBlock = async (
  sender: User,
  sec: string,
  option?: {
    expectedResult?: ExpectedTxResult;
  }
) => {
  const addressPlayerCore = (await ethers.getContract(ContractId.PlayerCore)) as PlayerCore;
  const {expectedResult} = option || {};
  if (expectedResult?.revertMessage) {
    await expect(
      addressPlayerCore.connect(await ethers.getSigner(sender.address)).setSecondsPerBlock(sec)
    ).to.be.revertedWith(expectedResult.revertMessage);
    return {} as ContractReceipt;
  }
  return await addressPlayerCore.connect(await ethers.getSigner(sender.address)).setSecondsPerBlock(sec);
};

export const getSecondsPerBlock = async (sender: User) => {
  const addressPlayerCore = (await ethers.getContract(ContractId.PlayerCore)) as PlayerCore;
  return await addressPlayerCore.connect(await ethers.getSigner(sender.address)).secondsPerBlock();
};

export const setAdmin = async (
  sender: User,
  newAdmin: User,
  option?: {
    expectedResult?: ExpectedTxResult;
  }
) => {
  const addressPlayerCore = (await ethers.getContract(ContractId.PlayerCore)) as PlayerCore;
  const {expectedResult} = option || {};
  if (expectedResult?.revertMessage) {
    await expect(
      addressPlayerCore.connect(await ethers.getSigner(sender.address)).setAdmin(newAdmin.address)
    ).to.be.revertedWith(expectedResult.revertMessage);
    return {} as ContractReceipt;
  }
  await addressPlayerCore.connect(await ethers.getSigner(sender.address)).setAdmin(newAdmin.address);

  expect(await getAdmin(sender)).to.equal(newAdmin.address);
};

export const setBoss = async (
  sender: User,
  newBoss: User,
  option?: {
    expectedResult?: ExpectedTxResult;
  }
) => {
  const addressPlayerCore = (await ethers.getContract(ContractId.PlayerCore)) as PlayerCore;
  const {expectedResult} = option || {};
  if (expectedResult?.revertMessage) {
    await expect(
      addressPlayerCore.connect(await ethers.getSigner(sender.address)).setBoss(newBoss.address)
    ).to.be.revertedWith(expectedResult.revertMessage);
    return {} as ContractReceipt;
  }
  await addressPlayerCore.connect(await ethers.getSigner(sender.address)).setBoss(newBoss.address);

  expect(await getBoss(sender)).to.equal(newBoss.address);
};

export const getAdmin = async (sender: User) => {
  const addressPlayerCore = (await ethers.getContract(ContractId.PlayerCore)) as PlayerCore;
  return await addressPlayerCore.connect(await ethers.getSigner(sender.address)).adminAddress();
};

export const getBoss = async (sender: User) => {
  const addressPlayerCore = (await ethers.getContract(ContractId.PlayerCore)) as PlayerCore;
  return await addressPlayerCore.connect(await ethers.getSigner(sender.address)).bossAddress();
};

export const checkPause = async (sender: User) => {
  const addressPlayerCore = (await ethers.getContract(ContractId.PlayerCore)) as PlayerCore;
  return await addressPlayerCore.connect(await ethers.getSigner(sender.address)).paused();
};
export const setPause = async (sender: User): Promise<void> => {
  const addressPlayerCore = (await ethers.getContract(ContractId.PlayerCore)) as PlayerCore;
  await addressPlayerCore.connect(await ethers.getSigner(sender.address)).pause();
  expect(await checkPause(sender)).to.equal(true);
};

export const setUnPause = async (
  sender: User,
  option?: {
    expectedResult?: ExpectedTxResult;
  }
): Promise<void> => {
  const addressPlayerCore = (await ethers.getContract(ContractId.PlayerCore)) as PlayerCore;
  const {expectedResult} = option || {};
  if (expectedResult?.revertMessage) {
    await expect(addressPlayerCore.connect(await ethers.getSigner(sender.address)).unpause()).to.be.revertedWith(
      expectedResult.revertMessage
    );
    return;
  }
  await addressPlayerCore.connect(await ethers.getSigner(sender.address)).unpause();
  expect(await checkPause(sender)).to.equal(false);
};

// export const contractBalance = async (sender: User) => {
//   const addressPlayerCore = (await ethers.getContract(ContractId.PlayerCore)) as PlayerCore;
//   console.log('1', await addressPlayerCore.balanceOf(addressPlayerCore.address));
//   addressPlayerCore.balance()
//   return await (await ethers.getDefaultProvider()).getBalance(addressPlayerCore.address);
// };

export const payContract = async (sender: User, eth: string) => {
  const addressPlayerCore = (await ethers.getContract(ContractId.PlayerCore)) as unknown as PlayerCore;
  const txResult = await (
    await ethers.getSigner(sender.address)
  ).sendTransaction({to: addressPlayerCore.address, value: ethers.utils.parseEther(eth)});

  return txResult;
};

export const isPregnant = async (sender: User, playerId: string) => {
  const addressPlayerCore = (await ethers.getContract(ContractId.PlayerCore)) as unknown as PlayerCore;
  return await addressPlayerCore.connect(await ethers.getSigner(sender.address)).isPregnant(playerId);
};

export const getAutoBirthFee = async (sender: User) => {
  const addressPlayerCore = (await ethers.getContract(ContractId.PlayerCore)) as unknown as PlayerCore;
  return await addressPlayerCore.connect(await ethers.getSigner(sender.address)).autoBirthFee();
};

export const setAutoBirthFee = async (sender: User, fee: BigNumber) => {
  const addressPlayerCore = (await ethers.getContract(ContractId.PlayerCore)) as unknown as PlayerCore;
  await addressPlayerCore.connect(await ethers.getSigner(sender.address)).setAutoBirthFee(fee);
  expect(await getAutoBirthFee(sender)).to.equal(fee);
};

export const approve = async (sender: User, to: string, playerId: string) => {
  const addressPlayerCore = (await ethers.getContract(ContractId.PlayerCore)) as unknown as PlayerCore;
  return await addressPlayerCore.connect(await ethers.getSigner(sender.address)).approve(to, playerId);
};

export const transferFrom = async (sender: User, from: string, to: string, playerId: string) => {
  const addressPlayerCore = (await ethers.getContract(ContractId.PlayerCore)) as unknown as PlayerCore;
  return await addressPlayerCore.connect(await ethers.getSigner(sender.address)).transferFrom(from, to, playerId);
};
