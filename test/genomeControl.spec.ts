import {BigNumber} from '@ethersproject/bignumber';
import {expect} from 'chai';
import {ethers} from 'hardhat';
import {stringify} from 'querystring';
import {GenomeControl} from '../typechain/GenomeControl';
import {ContractId, Fixture, ProtocolErrors} from '../types';
import {deployGenome} from '../utils/contractDeployer';
import {advanceNBlock} from '../utils/hhNetwork';
import {getPlayer, getGenomeControl, rand, setGenomeControl, withdrawBalance} from '../utils/protocolTx';
import {setupFixture} from '../utils/setupFixture';

describe('PlayerCore ', function () {
  const fixture = {} as Fixture;
  before(async () => {
    Object.assign(fixture, await setupFixture());
  });

  it('test rand', async function () {
    const {admin} = fixture;
    let randArray = await rand(admin, 0 as unknown as BigNumber, 0 as unknown as BigNumber, 1 as unknown as BigNumber);
    console.log(randArray);
    await advanceNBlock(5);
    randArray = await rand(admin, 0 as unknown as BigNumber, 0 as unknown as BigNumber, 1 as unknown as BigNumber);
    console.log(randArray);
  });
});
