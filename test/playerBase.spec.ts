import {expect} from 'chai';
import {ethers} from 'hardhat';
import {stringify} from 'querystring';
import {GenomeControl} from '../typechain/GenomeControl';
import {ContractId, Fixture, ProtocolErrors} from '../types';
import {deployGenome} from '../utils/contractDeployer';
import {
  approve,
  balanceOf,
  getPlayer,
  getGenomeControl,
  getSecondsPerBlock,
  setGenomeControl,
  setSecondsPerBlock,
  tokensOfOwner,
  transfer,
  transferFrom,
} from '../utils/protocolTx';
import {setupFixture} from '../utils/setupFixture';

describe('PlayerBase ', function () {
  const fixture = {} as Fixture;
  before(async () => {
    Object.assign(fixture, await setupFixture());
  });

  it('transfer player', async function () {
    const {admin, user4} = fixture;
    const balBefore = await balanceOf(user4, user4);
    await transfer(admin, user4, admin.ownPlayerIds[0]);
    const balAfter = await balanceOf(user4, user4);

    expect(balAfter.sub(balBefore)).to.equal(1);
  });

  it('get tokensOfOwner ', async function () {
    const {user1} = fixture;
    const tokens = await tokensOfOwner(user1, user1);

    expect(tokens.length).to.equal(2);
  });

  it('get tokensOfOwner 2', async function () {
    const {user5} = fixture;
    const tokens = await tokensOfOwner(user5, user5);

    expect(tokens.length).to.equal(0);
  });

  it('setSecondsPerBlock', async function () {
    const {boss} = fixture;
    await setSecondsPerBlock(boss, '16');
    const currentSecondsPerBlock = await getSecondsPerBlock(boss);

    expect(currentSecondsPerBlock).to.equal(16);
  });

  it('setSecondsPerBlock', async function () {
    const {admin} = fixture;
    await setSecondsPerBlock(admin, '17');
    const currentSecondsPerBlock = await getSecondsPerBlock(admin);

    expect(currentSecondsPerBlock).to.equal(17);
  });

  it('setSecondsPerBlock', async function () {
    const {user1} = fixture;
    await setSecondsPerBlock(user1, '16', {expectedResult: {revertMessage: ProtocolErrors.ONLY_CONTROL}});
  });

  it('approve', async function () {
    const {user1, user2} = fixture;
    await approve(user2, user1.address, user2.ownPlayerIds[0]);
  });

  it('transferFrom', async function () {
    const {user1, user2, user3} = fixture;
    await approve(user2, user1.address, user2.ownPlayerIds[1]);
    await transferFrom(user1, user2.address, user3.address, user2.ownPlayerIds[1]);
  });
});
