import { BigNumber } from '@ethersproject/bignumber';
import {expect} from 'chai';
import {ethers} from 'hardhat';
import {stringify} from 'querystring';
import {GenomeControl} from '../typechain/GenomeControl';
import {ContractId, Fixture, ProtocolErrors} from '../types';
import {FINNEY} from '../utils/constant';
import {deployGenome} from '../utils/contractDeployer';
import {getGenomeControl, setAutoBirthFee, setGenomeControl} from '../utils/protocolTx';
import {setupFixture} from '../utils/setupFixture';

describe('PlayerBreeding GenomeControl contract get/set', function () {
  const fixture = {} as Fixture;
  let newGenomeControl: GenomeControl;
  before(async () => {
    Object.assign(fixture, await setupFixture());
    newGenomeControl = (await deployGenome('new')) as GenomeControl;
  });

  it('Should return the current GenomeControl address', async function () {
    const {user1} = fixture;
    const contractStoreGenomeControlAddress = await getGenomeControl();
    const currentGenomeControl = (await ethers.getContract(ContractId.GenomeControl)) as GenomeControl;

    expect(contractStoreGenomeControlAddress).to.equal(currentGenomeControl.address);
  });

  it('Chnage GenomeControl address with normal user', async function () {
    const {user1} = fixture;
    await setGenomeControl(user1, newGenomeControl, {expectedResult: {revertMessage: ProtocolErrors.ONLY_BOSS}});
  });

  it('Chnage GenomeControl address with Boss user', async function () {
    const {boss} = fixture;
    const contractStoreGenomeControlAddress = await getGenomeControl();
    await setGenomeControl(boss, newGenomeControl);
    const newContractStoreGenomeControlAddress = await getGenomeControl();
    expect(contractStoreGenomeControlAddress).to.not.equal(newContractStoreGenomeControlAddress);
    expect(newContractStoreGenomeControlAddress).to.equal(newGenomeControl.address);
  });

  it('setAutoBirthFee', async function () {
    const {admin} = fixture;
    await setAutoBirthFee(admin, (FINNEY * 2) as unknown as BigNumber);
  });
});
