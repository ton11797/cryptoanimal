import {expect} from 'chai';
import {BigNumber} from 'ethers';
import {ethers} from 'hardhat';
import {stringify} from 'querystring';
import {GenomeControl} from '../typechain/GenomeControl';
import {ContractId, Fixture, ProtocolErrors} from '../types';
import {FINNEY} from '../utils/constant';
import {advanceBlock, advanceNBlock, advanceTimeAndBlock} from '../utils/hhNetwork';
import {balanceOf, breedWithAuto, getPlayer, giveBirth, isPregnant} from '../utils/protocolTx';
import {setupFixture} from '../utils/setupFixture';

describe('Player flow ', function () {
  const fixture = {} as Fixture;
  before(async () => {
    Object.assign(fixture, await setupFixture());
  });

  it('Owner breeding', async function () {
    const {admin} = fixture;
    let isAPregnanted = await isPregnant(admin, admin.ownPlayerIds[0]);
    let isBPregnanted = await isPregnant(admin, admin.ownPlayerIds[1]);
    expect(isAPregnanted).to.equal(false);
    expect(isBPregnanted).to.equal(false);
    await breedWithAuto(admin, admin.ownPlayerIds[0], admin.ownPlayerIds[1], (2 * FINNEY) as unknown as BigNumber);
    isAPregnanted = await isPregnant(admin, admin.ownPlayerIds[0]);
    isBPregnanted = await isPregnant(admin, admin.ownPlayerIds[1]);
    expect(isAPregnanted).to.equal(false);
    expect(isBPregnanted).to.equal(true);
    await advanceNBlock(5);
    const playerId = await giveBirth(admin, admin.ownPlayerIds[1]);
    const info = await getPlayer(admin, playerId);
    expect(info.partnerAId).to.equal(admin.ownPlayerIds[0]);
    expect(info.partnerBId).to.equal(admin.ownPlayerIds[1]);
  });
});
