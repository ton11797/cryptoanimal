import {expect} from 'chai';
import {ethers} from 'hardhat';
import {stringify} from 'querystring';
import {GenomeControl} from '../typechain/GenomeControl';
import {ContractId, Fixture, ProtocolErrors} from '../types';
import {deployGenome} from '../utils/contractDeployer';
import {getPlayer, getGenomeControl, payContract, setGenomeControl, withdrawBalance} from '../utils/protocolTx';
import {setupFixture} from '../utils/setupFixture';

describe('PlayerCore ', function () {
  const fixture = {} as Fixture;
  before(async () => {
    Object.assign(fixture, await setupFixture());
  });

  it('Should return the information about a specific player id 0', async function () {
    const {user1} = fixture;
    const MAX_INT = '115792089237316195423570985008687907853269984665640564039457584007913129639935';

    const info = await getPlayer(user1, '0');

    expect(info.generation).to.equal(0);
    expect(info.cooldownIndex).to.equal(0);
    expect(info.chromosomeA).to.equal(MAX_INT);
    expect(info.chromosomeB).to.equal(MAX_INT);
    expect(info.chromosomeC).to.equal(MAX_INT);
    expect(info.pregnantWithId).to.equal(0);
    expect(info.partnerBId).to.equal(0);
    expect(info.partnerAId).to.equal(0);
  });

  it('withdrawBalance', async function () {
    const {admin} = fixture;
    const MAX_INT = '115792089237316195423570985008687907853269984665640564039457584007913129639935';

    const info = await withdrawBalance(admin);
  });

  it('contractBalance', async function () {
    const {admin} = fixture;
    await payContract(admin, '1');
    await withdrawBalance(admin);
  });
});
