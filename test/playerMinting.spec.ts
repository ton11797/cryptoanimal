import {BigNumber} from '@ethersproject/bignumber';
import {expect} from 'chai';
import {Fixture, ProtocolErrors, User} from '../types';
import {USER_ZERRO} from '../utils/constant';
import {createPromoPlayer, ownerOf} from '../utils/protocolTx';
import {setupFixture} from '../utils/setupFixture';

describe('PlayerMinting ', function () {
  const fixture = {} as Fixture;
  before(async () => {
    Object.assign(fixture, await setupFixture());
  });

  it('createPromoPlayer by admin to admin', async function () {
    const {admin} = fixture;
    const genome = 0 as unknown as BigNumber;
    const playerId = await createPromoPlayer(admin, admin, genome, genome, genome);
    const owner = await ownerOf(admin, playerId);

    expect(owner).to.equal(admin.address);
  });

  it('createPromoPlayer by admin to user1', async function () {
    const {admin, user1} = fixture;
    const genome = 0 as unknown as BigNumber;
    const playerId = await createPromoPlayer(admin, user1, genome, genome, genome);
    const owner = await ownerOf(admin, playerId);

    expect(owner).to.equal(user1.address);
  });

  it('createPromoPlayer by admin to 0', async function () {
    const {admin} = fixture;
    const genome = 0 as unknown as BigNumber;
    const playerId = await createPromoPlayer(admin, USER_ZERRO, genome, genome, genome);
    const owner = await ownerOf(admin, playerId);

    expect(owner).to.equal(admin.address);
  });

  it('createPromoPlayer by normal user', async function () {
    const {user1} = fixture;
    const genome = 0 as unknown as BigNumber;
    await createPromoPlayer(user1, user1, genome, genome, genome, {
      expectedResult: {revertMessage: ProtocolErrors.ONLY_ADMIN},
    });
  });
});
