import {expect} from 'chai';
import {BigNumber} from 'ethers';
import {ethers} from 'hardhat';
import {stringify} from 'querystring';
import {GenomeControl} from '../typechain/GenomeControl';
import {ContractId, Fixture, ProtocolErrors} from '../types';
import {FINNEY, USER_ZERRO} from '../utils/constant';
import {deployGenome} from '../utils/contractDeployer';
import {advanceBlock, advanceNBlock, advanceTimeAndBlock} from '../utils/hhNetwork';
import {
  balanceOf,
  breedWithAuto,
  getAdmin,
  getPlayer,
  getBoss,
  getGenomeControl,
  giveBirth,
  setAdmin,
  setBoss,
  setGenomeControl,
  setPause,
  setUnPause,
} from '../utils/protocolTx';
import {setupFixture} from '../utils/setupFixture';

describe('Player flow ', function () {
  const fixture = {} as Fixture;
  before(async () => {
    Object.assign(fixture, await setupFixture());
  });

  it('Set new boss', async function () {
    const {boss, user1} = fixture;
    await setBoss(boss, USER_ZERRO, {expectedResult: {revertMessage: ProtocolErrors.ADDRESS_ZEROP}});
    await setBoss(boss, user1);

    //clearup
    await setBoss(user1, boss);
  });

  it('Set new admin', async function () {
    const {boss, user1, admin} = fixture;
    await setAdmin(boss, USER_ZERRO, {expectedResult: {revertMessage: ProtocolErrors.ADDRESS_ZEROP}});
    await setAdmin(boss, user1);

    //clearup
    await setAdmin(boss, admin);
  });

  it('setPause', async function () {
    const {admin, boss} = fixture;
    await setPause(boss);
    await setUnPause(admin, {expectedResult: {revertMessage: ProtocolErrors.ONLY_BOSS}});
    //clearup
    await setUnPause(boss);
  });
});
